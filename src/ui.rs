#![allow(non_snake_case)]
// import the prelude to get access to the `rsx!` macro and the `Scope` and `Element` types
use dioxus::prelude::*;

// define a component that renders a div with the text "Hello, world!"
pub fn App(cx: Scope) -> Element {
    let servers = use_future(cx, (), |_| async move { crate::matrix::logic().await });
    let Some(servers) = servers.value() else {
        return cx.render(rsx! {
            p {
                "Loading..."
            }
        })
    };
    let active_server = use_state(cx, || servers[0].clone());
    let active_channel = use_state(cx, || active_server.channels[0].clone());

    cx.render(rsx! {
        div {
            class: "app",
            ServerList {
                servers: &servers,
                active_server: active_server.get(),
                on_select: move |index| {
                    let server: &crate::matrix::Server = &servers[index];
                    active_server.set(server.clone());
                    active_channel.set(server.channels[0].clone());
                },
            },
            ChannelList {
                server: active_server.get(),
                active_channel: active_channel.get(),
                on_select: move |index| {
                    let channel: &crate::matrix::Channel = &active_server.channels[index];
                    active_channel.set(channel.clone());
                },
            },
            MessageView {
                channel: active_channel.get(),
            },
        }
    })
}

#[inline_props]
fn ServerList<'a>(
    cx: Scope<'a>,
    active_server: &'a crate::matrix::Server,
    servers: &'a Vec<crate::matrix::Server>,
    on_select: EventHandler<'a, usize>,
) -> Element<'a> {
    cx.render(rsx! {
        div {
            class: "server-list",
            for (i, server) in servers.iter().enumerate() {
                ServerItem {
                    on_select: move |index| on_select.call(index),
                    index: i,
                    active: server.id == active_server.id,
                    url: "{server.avatar}",
                }
            }
        }
    })
}

#[inline_props]
fn ServerItem<'a>(
    cx: Scope<'a>,
    url: &'a str,
    active: bool,
    index: usize,
    on_select: EventHandler<'a, usize>,
) -> Element<'a> {
    let mut classes = "server-item".to_string();
    if *active {
        classes.push_str(" active");
    }
    cx.render(rsx! {
        div {
            onclick: move |_| on_select.call(*index),
            class: "{classes}",
            img {
                src: "{url}",
            },
        }
    })
}

#[inline_props]
fn ChannelList<'a>(
    cx: Scope<'a>,
    server: &'a crate::matrix::Server,
    active_channel: &'a crate::matrix::Channel,
    on_select: EventHandler<'a, usize>,
) -> Element<'a> {
    cx.render(rsx! {
        div {
            class: "channel-list",
            ServerTitle {
                title: &server.name,
            }
            div {
                class: "channel-items",
                for (i, channel) in server.channels.iter().enumerate() {
                    ChannelItem {
                        on_select: move |index| on_select.call(index),
                        channel: &channel,
                        active: channel.id == active_channel.id,
                        index: i,
                    }
                }
            }
        }
    })
}

#[inline_props]
fn ServerTitle<'a>(cx: Scope<'a>, title: &'a str) -> Element<'a> {
    cx.render(rsx! {
        div {
            class: "server-title",
            "{title}",
        }
    })
}

#[inline_props]
fn ChannelItem<'a>(
    cx: Scope<'a>,
    channel: &'a crate::matrix::Channel,
    active: bool,
    index: usize,
    on_select: EventHandler<'a, usize>,
) -> Element<'a> {
    let mut classes = "channel-item".to_string();
    if *active {
        classes.push_str(" active");
    }
    cx.render(rsx! {
        div {
            onclick: move |_| on_select.call(*index),
            class: "{classes}",
            IconTag {}
            "{channel.name}"
        }
    })
}

fn IconTag(cx: Scope) -> Element {
    cx.render(rsx! {
        svg {
            class: "icon-tag",
            width: "48px",
            height: "48px",
            view_box: "0 96 960 960",
            fill: "currentColor",
            path {
                d: "m250 896 43-170H140l15-60h153l45-180H180l15-60h173l42-170h59l-42 170h181l42-170h59l-42 170h153l-15 60H652l-45 180h173l-15 60H592l-42 170h-60l43-170H352l-42 170h-60Zm117-230h181l45-180H412l-45 180Z",
            }
        }
    })
}

#[inline_props]
fn MessageView<'a>(cx: Scope<'a>, channel: &'a crate::matrix::Channel) -> Element<'a> {
    cx.render(rsx! {
        div {
            class: "message-view",
            ChannelTitle {
                title: &channel.name
            },
            div {
                class: "message-items",
                for message in &channel.messages {
                    MessageItem {
                        message: message,
                    }
                }
            }
        }
    })
}

#[inline_props]
fn ChannelTitle<'a>(cx: Scope<'a>, title: &'a str) -> Element<'a> {
    cx.render(rsx! {
        div {
            class: "channel-title",
            IconTag {}
            "{title}"
        }
    })
}

#[inline_props]
fn MessageItem<'a>(cx: Scope<'a>, message: &'a crate::matrix::Message) -> Element<'a> {
    cx.render(rsx! {
        p {
            "{message.sender} at {message.created_at}:"
            br {}
            span {
                dangerous_inner_html: "{message.text}",
            }
        }
    })
}
