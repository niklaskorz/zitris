use dioxus::prelude::hot_reload_init;
use dioxus_desktop::WindowBuilder;
use dioxus_hot_reload::Config;

mod ui;
mod matrix;

fn main() {
    hot_reload_init!(Config::new().with_paths(&["src", "css"]));
    // launch the dioxus app in a webview
    let config = dioxus_desktop::Config::new()
        .with_window(WindowBuilder::new().with_title("Zitris"))
        .with_custom_head(
            r#"
            <link rel="stylesheet" href="css/modern-normalize.css">
            <link rel="stylesheet" href="css/style.css">
            "#.into(),
        );
    dioxus_desktop::launch_cfg(ui::App, config);
}
